AR-elastic-stack
=========

Task repository for elastic stack related information. See relevant inventory variables for vault information.

Requirements
------------

Docker Version : {{ Insert Value Here }}
Boto Version: {{ Insert Value Here }} 

Role Variables
--------------

Dependencies
------------

Example Playbook
----------------

License
-------

BSD

Author Information
------------------
